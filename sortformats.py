# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression"""

import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """
    index1 = fordered.index(format1)
    index2 = fordered.index(format2)
    if index1 < index2:
        return True
    else:
        return False


def find_lower_pos(formats: list, pivot: int) -> int:
    """Find the lower format in formats after pivot
    Returns the index of the lower format found"""
    lower = pivot
    for i in range(pivot + 1, len(formats)):
        if lower_than(formats[i], formats[lower]) is True:
            lower = i
    return lower

def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered"""

    for pivot in range(len(formats)): #

        derecha = find_lower_pos(formats, pivot)

        formats[pivot], formats[derecha] = formats[derecha], formats[pivot]

    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]

    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")

    sorted_formats: list = sort_formats(formats)  #Primero

    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()
